using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AIFindMaxPoint", menuName = "AI/AIFindMaxPoint")]

public class AIFindMaxPoint : AI
{
    [SerializeField] private int countNumber = 4;
    private List<Vector2Int> dirList = new() { Vector2Int.down, Vector2Int.left, Vector2Int.right, Vector2Int.up };
    private Dictionary<Vector2Int, int> paths;

    public override Vector2Int GetDirection(Vector2Int origin)
    {
        if (paths == null)
        {
            paths = new();
        }
        else
        {
            paths.Clear();
        }
        listPathPass.Clear();
        GetPath(origin, ref paths);

        Vector2Int result = new Vector2Int();
        int maxScore = 0;

        foreach (var p in paths)
        {
            if (p.Value > maxScore)
            {
                maxScore = p.Value;
                result = p.Key;
            }
        }

        return result;
    }

    private List<Vector2Int> listPathPass = new List<Vector2Int>();
    private Dictionary<Vector2Int, int> listScore = new();
    private void GetPath(Vector2Int origin, ref Dictionary<Vector2Int, int> _paths)
    {
        var listNextPoin = GetNextPoint(origin, listPathPass);
        if (listNextPoin != null && listNextPoin.Count > 0 && listPathPass.Count < countNumber)
        {
            listScore[origin] = listNextPoin.Count;
            for (int i = 0; i < listNextPoin.Count; i++)
            {
                Vector2Int p = listNextPoin[i];
                listPathPass.Add(p);
                GetPath(p, ref paths);
                if (i == listNextPoin.Count - 1)
                {
                    if (listPathPass.Count > 0)
                    {
                        listScore.Remove(listPathPass[listPathPass.Count - 1]);
                        listPathPass.Remove(listPathPass[listPathPass.Count - 1]);
                    }
                }
            }
        }
        else
        {
            if (listPathPass.Count > 0)
            {
                if (!_paths.ContainsKey(listPathPass[0]))
                {
                    _paths[listPathPass[0]] = TotalScore(listScore);
                }
                else
                {
                    if (listPathPass.Count > _paths[listPathPass[0]])
                    {
                        _paths[listPathPass[0]] = TotalScore(listScore);
                    }
                }
                listScore.Remove(listPathPass[listPathPass.Count - 1]);
                listPathPass.Remove(listPathPass[listPathPass.Count - 1]);
            }
        }
    }

    private int TotalScore(Dictionary<Vector2Int,int> dic)
    {
        int total = 0;
        foreach (var s in dic.Values)
        {
            total += s;
        }
        return total;
    }

    private List<Vector2Int> GetNextPoint(Vector2Int origin, List<Vector2Int> pathsPass = null)
    {
        // get list land can move for each origin
        List<Vector2Int> result = new();
        foreach (var dir in dirList)
        {
            var check = CheckCanMove(origin, dir, pathsPass);
            if (check.canMove)
            {
                result.Add(check.coordinate);
            }
        }
        return result;
    }

    private (bool canMove, Vector2Int coordinate) CheckCanMove(Vector2Int origin, Vector2Int direction, List<Vector2Int> pathsPass = null)
    {
        var coor = origin + direction;
        if (pathsPass != null)
        {
            if (pathsPass.Contains(coor))
            {
                return (false, coor);
            }
        }

        LandUnit l = LandManager.Instance.GetLand(coor);
        if (l != null)
        {
            if (l.CanMove())
            {
                return (true, coor);
            }
        }
        return (false, coor);
    }
}
