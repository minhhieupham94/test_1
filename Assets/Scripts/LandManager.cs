using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LandManager : MonoBehaviour
{
    public static LandManager Instance;
    [SerializeField] private int collum;
    [SerializeField] private int raw;
    [SerializeField] private LandUnit landPrefab;
    [SerializeField] private Transform landContain;
    [SerializeField] private List<Vector2Int> obstacleCoorListInput;
    public List<Vector2Int> obstacleCoorList { get; private set; }
    public Dictionary<Vector2Int, LandUnit> ListLand { get; private set; }
    public void Initialized()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        if(ListLand == null)
        {
            ListLand = new();
        }
        else
        {
            foreach (var L in ListLand)
            {
                Destroy(L.Value);
            }
            ListLand.Clear();
        }
        
        obstacleCoorList ??= new();
        GetListObstacle();
        LoadMap();
    }

    public void GetListObstacle()
    {
        if(obstacleCoorListInput != null)
        {
            foreach (var c in obstacleCoorListInput)
            {
                if (!obstacleCoorList.Contains(c))
                    obstacleCoorList.Add(c);
                var symmetryPosition = GetSymmetryPosition(c);
                if(!obstacleCoorList.Contains(symmetryPosition))
                {
                    obstacleCoorList.Add(symmetryPosition);
                }
            }
        }
        
    }

    public void LoadMap()
    {
        for (int i = 0; i < collum; i++)
        {
            for (int j = 0; j < raw; j++)
            {
                LandUnit l = null;
                if(GetLand(new Vector2Int(i,j)) == null)
                {
                    l = Instantiate(landPrefab, landContain);
                }
                else
                {
                    l = GetLand(new Vector2Int(i, j));
                }
               
                var landData = new LandData()
                {
                    coordinate = new Vector2Int(i, j),
                    isCollect = false,
                };
                landData.SetKind(KindLand.Land);
                if (obstacleCoorList != null)
                {
                    if(obstacleCoorList.Contains(landData.coordinate))
                    {
                        landData.SetKind(KindLand.Obstacle);
                    }
                }
                l.Initialize(landData);
                l.OnclickRegister(OnClick);
                ListLand[new Vector2Int(i, j)] = l;
            }
        }
    }

    public Vector2Int GetSymmetryPosition(Vector2Int _position)
    {
        Vector2 center = new Vector2(collum / 2, raw / 2);
        return new Vector2Int((int)(2 * center.x - _position.x),(int)( 2 * center.y - _position.y));
    }

    public Vector2 GetPosition(Vector2Int _coordinate)
    {
        if(ListLand.ContainsKey(_coordinate))
        {
            return ListLand[_coordinate].GetPostition();
        }
        return Vector2.zero;
    }

    public LandUnit GetLand(Vector2Int coor)
    {
        if(ListLand.ContainsKey(coor))
        {
            return ListLand[coor];
        }
        return null;
    }

    public void OnClick(Vector2Int _coordinate)
    {
        GameManager.Instance.UI.SetTimeCoolDown(0);
        GameManager.Instance.MoveCharacter(_coordinate);
    }
    private List<Vector2Int> dirList = new() { Vector2Int.down, Vector2Int.left, Vector2Int.right, Vector2Int.up };
    public bool CheckMove(Vector2Int _coordinate)
    {
        foreach (var dir in dirList)
        {
            var coor = _coordinate + dir;
            var l = GetLand(coor);
            if (l != null)
            {
                if (l.IsLand() && !l.IsCollect())
                {
                    return true;
                }
            }
        }

        return false;
    }

    public LandUnit GetLandMove(Vector2Int _coordinate)
    {
        foreach (var dir in dirList)
        {
            var coor = _coordinate + dir;
            var l = GetLand(coor);
            if (l != null)
            {
                if (l.IsLand() && !l.IsCollect())
                {
                    return l;
                }
            }
        }

        return null;
    }
}
