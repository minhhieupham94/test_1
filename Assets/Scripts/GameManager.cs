using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    [SerializeField] private bool yourTurn;
    [SerializeField] LandManager _landManager;
    [SerializeField] private UI ui;
    [SerializeField] private Vector2Int startPosition;
    [SerializeField] private Character characterPrefab;
    [SerializeField] private Transform characterContain;
    [SerializeField] private OpponentInput opponentInput;
    private Character player;
    private Character opponent;
    public bool YourTurn { get; set; }
    public bool EndGame { get; set; }

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        YourTurn = yourTurn;
    }

    private void SetStartPosition()
    {
        if (player == null)
            player = Instantiate(characterPrefab, characterContain);
        var playerData = new CharacterData()
        {
            StartPosition = startPosition,
            IsPlayer = true,
        };
        player.Initialized(playerData);
        var l1 = _landManager.GetLand(startPosition);
        if (l1 != null)
        {
            l1.SetLandKind(KindLand.Start);
        }
        if (opponent == null)
            opponent = Instantiate(characterPrefab, characterContain);
        var opponentData = new CharacterData()
        {
            StartPosition = LandManager.Instance.GetSymmetryPosition(startPosition),
            IsPlayer = false,
        };
        opponent.Initialized(opponentData);
        var l2 = _landManager.GetLand(opponentData.StartPosition);
        if (l2 != null)
        {
            l2.SetLandKind(KindLand.Start);
        }
    }

    public void StartGame()
    {
        EndGame = false;
        _landManager.Initialized();
        SetStartPosition();
    }

    public void ChangeTurn()
    {
        YourTurn = !YourTurn;
        if(!YourTurn)
        {
            opponentInput.Input();
        }
    }

    public UI UI => ui;

    public Character Player => player;
    public Character Opponent => opponent;
    public bool IsMoving { get; set; }
    private bool playerCanMove = true;
    private bool opponentCanMove = true;
    public void MoveCharacter(Vector2Int _coodinate)
    {
        IsMoving = true;
        if (YourTurn)
        {
            Player.DoMove(_coodinate,() =>
            {
                IsMoving = false;
                playerCanMove = _landManager.CheckMove(Player.Coordinate);
                CheckEndGame(true);
            });
        }
        else
        {
            opponent.DoMove(_coodinate, () =>
            {
                IsMoving = false;
                opponentCanMove = _landManager.CheckMove(Opponent.Coordinate);
                CheckEndGame(false);
            }
            );
        }
    }

    public void CheckEndGame(bool _player)
    {
        if (playerCanMove && opponentCanMove) return;
        string txt = "";
        if (!playerCanMove && !opponentCanMove)
        {
            if (Player.Steps == Opponent.Steps)
            {
                txt = "Draw !!!";
                EndGame = true;
            }
        }
        if(!EndGame && !playerCanMove)
        {
            if (Player.Steps < Opponent.Steps)
            {
                txt = "Opponent Win !!!";
                EndGame = true;
            }
        }
        if (!EndGame && !opponentCanMove)
        {
            if (Player.Steps > Opponent.Steps)
            {
                txt = "You Win !!!";
                EndGame = true;
            }
        }
        if (EndGame)
            ui.EndGame(txt);
        
    }
}
