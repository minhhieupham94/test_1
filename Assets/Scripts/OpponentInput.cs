using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum State
{
    NORMAL,
    ATTACK,
    DEFEND,
}

public class OpponentInput : MonoBehaviour
{
    public AI AiNormal;
    public AI AiAttack;
    public void Input()
    {
        if (GameManager.Instance.EndGame) return;
        StartCoroutine(InputYield());
    }

    IEnumerator InputYield()
    {
        yield return new WaitForSeconds(0.5f);
        LandUnit unit = null;
        var curState = CheckState();
        Vector2Int direction = new();
        
        switch (curState)
        {
            case State.NORMAL:
                {
                    direction = AiNormal.GetDirection(GameManager.Instance.Opponent.Coordinate);
                    
                    break;
                }
            case State.ATTACK:
                {
                    direction = AiAttack.GetDirection(GameManager.Instance.Opponent.Coordinate);
                    break;
                }
        }
        unit = LandManager.Instance.GetLand(direction);
       
        if (unit != null)
        {
            unit.OnClick(false);
        }
    }

    private State CheckState()
    {
        var point = GameManager.Instance.Opponent.Steps + GameManager.Instance.Player.Steps;
        var map = LandManager.Instance.ListLand.Count - LandManager.Instance.obstacleCoorList.Count;
        if (point >= map *1f * 1/3)
        {
            return State.ATTACK;
        }
        else
        {
            return State.NORMAL;
        }
    }
}
