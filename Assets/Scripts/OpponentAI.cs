using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "OpponentAI",menuName = "AI/OpponentAI")]
public class OpponentAI : AI
{
    [SerializeField] private int countNumber = 4;
    private List<Vector2Int> dirList = new() { Vector2Int.down, Vector2Int.left, Vector2Int.right, Vector2Int.up };
    private Dictionary<Vector2Int, List<Vector2Int>> paths;

    public override Vector2Int GetDirection(Vector2Int origin)
    {
        if (paths == null)
        {
            paths = new();
        }
        else
        {
            paths.Clear();
        }

        foreach (var dir in dirList)
        {
            var check = CheckCanMove(origin, dir);
            if (check.canMove)
            {
                paths[dir] = new List<Vector2Int>() { check.coordinate };
            }
        }

        if (paths.Count == 0) return Vector2Int.zero;

        Dictionary<Vector2Int, List<Vector2Int>> temporyPath = new Dictionary<Vector2Int, List<Vector2Int>>();
        bool hasPath;
        for (int i = 0; i < countNumber; i++)
        {
            temporyPath.Clear();
            hasPath = false;
            foreach (var path in paths)
            {
                temporyPath[path.Key] = new();

                foreach (var point in path.Value)
                {
                    temporyPath[path.Key].AddRange(GetNextPoint(point));
                }

                if (temporyPath[path.Key].Count > 0)
                {
                    hasPath = true;
                }
            }

            if (hasPath)
            {
                foreach (var p in temporyPath)
                {
                    if (p.Value.Count == 0)
                    {
                        paths.Remove(p.Key);
                    }
                    else
                    {
                        paths[p.Key] = p.Value;
                    }
                }
            }

            if (paths.Count == 1 || !hasPath || i == countNumber - 1)
            {
                foreach (var d in paths)
                {
                    return d.Key;
                }
            }

        }


        return new Vector2Int();
    }

    private List<Vector2Int> GetNextPoint(Vector2Int origin)
    {
        // get list land can move for each origin
        List<Vector2Int> result = new();
        foreach (var dir in dirList)
        {
            var check = CheckCanMove(origin, dir);
            if (check.canMove)
            {
                result.Add(check.coordinate);
            }
        }
        return result;
    }

    private (bool canMove, Vector2Int coordinate) CheckCanMove(Vector2Int origin, Vector2Int direction)
    {
        var coor = origin + direction;
        LandUnit l = LandManager.Instance.GetLand(coor);
        if (l != null)
        {
            if (l.CanMove())
            {
                return (true,coor);
            }
        }
        return (false,coor);
    }
}
