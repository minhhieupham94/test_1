using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    [SerializeField] private Text text;
    [SerializeField] private GameObject buttonStart;
    [SerializeField] private int countTime = 3;
    private int timeCoolDown;

    public void SetText(string str)
    {
        text.text = str;
    }

    public void OnStart()
    {
        GameManager.Instance.StartGame();
        buttonStart.SetActive(false);
        InvokeRepeating("CoolDown", 1, 1);
        SetTimeCoolDown(0);
    }

    public void EndGame(string txt)
    {
        buttonStart.SetActive(true);
        SetText(txt);
    }

    public void CoolDown()
    {
        if(GameManager.Instance.EndGame)
        {
            CancelInvoke("CoolDown");
            return;
        }
        string txt = "";
        timeCoolDown--;
        if(timeCoolDown >= 0 )
        {
            if(GameManager.Instance.YourTurn)
            {
                txt = $"{timeCoolDown}";
            }
        }
        else
        {
            GameManager.Instance.ChangeTurn();
            if (GameManager.Instance.YourTurn)
            {
                txt = "Your Turn !";
            }
            else
            {
                txt = "Opponent !";
            }
            timeCoolDown = countTime;
        }
        SetText(txt);
    }

    public void SetTimeCoolDown(int val)
    {
        timeCoolDown = val;
    }
}
