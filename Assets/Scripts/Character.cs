using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterData
{
    public Vector2Int StartPosition;
    public bool IsPlayer;
}

public class Character : MonoBehaviour
{
    private Vector2Int curCoor;
    private int steps;
    private CharacterData data;
    public Sprite OpponentSpr;
    private Image image;
    public void Initialized(CharacterData _data)
    {
        if (image == null) image = GetComponent<Image>();
        data = _data;
        if(data != null)
        {
            if (!data.IsPlayer) image.sprite = OpponentSpr;
            SetPosition(data.StartPosition);
            steps = 0;
        }
    }

    public void SetPosition(Vector2Int _coodinate)
    {
        curCoor = _coodinate;
        var pos = LandManager.Instance.GetPosition(_coodinate);
        transform.position = new Vector3(pos.x, pos.y, 0);
    }

    public void AddStep()
    {
        steps++;
    }

    public int Steps => steps;

    public void DoMove(Vector2Int _coodinate,Action callback = null)
    {
        SetPosition(_coodinate);
        AddStep();
        callback?.Invoke();
    }

    public Vector2Int Coordinate => curCoor;
}
