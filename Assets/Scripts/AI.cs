using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AI : ScriptableObject
{
    public abstract Vector2Int GetDirection(Vector2Int origin);
  
}
