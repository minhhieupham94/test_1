using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum KindLand
{
    Land,
    Obstacle,
    Start,
}

public class LandData
{
    public Vector2Int coordinate;
    public KindLand Kind;  //0: land || 1: obstacle
    public bool isCollect; 

    public void SetKind(KindLand _kind)
    {
        Kind = _kind;
    }
}

public class LandUnit : MonoBehaviour
{
    [SerializeField] private Color landColor;
    [SerializeField] private Color obstacleColor;
    [SerializeField] private Color startColor;
    [SerializeField]private Button button;
    private LandData data;
    private Image image;
    private RectTransform rectTrf;
    
    public void Initialize(LandData _data)
    {
        if(image == null)
        {
            image = GetComponent<Image>();
        }
        
        if(rectTrf == null)
        {
            rectTrf = GetComponent<RectTransform>();
        }

        data = _data;
        if(_data != null)
        {
            SetPostition(data.coordinate);
            SetLandKind(_data.Kind);
            SetCollect(false);
        }
    }

    private void SetPostition(Vector2Int _coordinate)
    {
        rectTrf.localPosition = _coordinate * rectTrf.rect.size;
    }

    public void SetLandKind(KindLand _kind)
    {
        if(_kind == KindLand.Land)
        {
            image.color = landColor;
            button.interactable = true;
        }
        else if(_kind == KindLand.Obstacle)
        {
            image.color = obstacleColor;
            button.interactable = false;
        }
        else if (_kind == KindLand.Start)
        {
            image.color = startColor;
            button.interactable = false;
            data.SetKind(_kind);
        }
    }

    public void SetCollect(bool val)
    {
        if (data != null)
            data.isCollect = val;
    }

    public bool IsCollect()
    {
        if (data != null)
            return data.isCollect;
        return true;
    }

    public bool IsLand()
    {
        return data.Kind == KindLand.Land;
    }

    public Vector2 GetPostition()
    {
        return rectTrf.position;
    }

    public Vector2Int GetCoordinate()
    {
        if(data != null)
        {
            return data.coordinate;
        }
        else
        {
            Debug.LogError($"data LandUnit is null!!!");
            return new Vector2Int();
        }
    }

    private Action<Vector2Int> onClickAction;

    public void OnclickRegister(Action<Vector2Int> act)
    {
        if(act != null)
        {
            onClickAction += act;
        }
    }
    public void OnclickUnRegister()
    {
        if (onClickAction != null)
        {
            onClickAction = null;
        }
    }

    public void OnClick(bool _player)
    {
        if (!GameManager.Instance.YourTurn && _player) return;

        if (GameManager.Instance.YourTurn && !_player) return;

        if (GameManager.Instance.EndGame) return;

        if (GameManager.Instance.IsMoving) return;

        if (Vector2.Distance(data.coordinate, GameManager.Instance.Player.Coordinate) != 1 && _player) return;

        if (Vector2.Distance(data.coordinate, GameManager.Instance.Opponent.Coordinate) != 1 && !_player) return;
        onClickAction?.Invoke(data.coordinate);
        OnclickUnRegister();
        SetCollect(true);
        button.interactable = false;
    }

    public bool CanMove()
    {
        return IsLand() && !IsCollect();
    }
}
